//
//  vlxNetworkTraceTester.h
//  veloxitySDK
//
//  Created by Mert Güneş on 02/10/14.
//  Copyright (c) 2014 Mert Güneş. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol vlxNetworkTraceTesterDelegate <NSObject>

/**
 * Delegate function which is called after the network trace test is done.
 */
- (void)didFinishNetworkTrace:(NSMutableDictionary*)_sampleArray;

@end

@interface vlxNetworkTraceTester : NSObject

@property (nonatomic) id<vlxNetworkTraceTesterDelegate> delegate;

/**
 * Makes a network trace test to the given address. Calls didFinishNetworkTrace: delegate method with an array which contains the alive IPs on the network.
 */
- (void)startNetworkTrace;

@end
